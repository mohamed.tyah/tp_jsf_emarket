/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.io.Serializable;
import java.util.ArrayList;
import model.Product;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author mac
 */
@ManagedBean
@ApplicationScoped
public class CatalogManager implements Serializable {
    private ArrayList<Product> catalog;
    private int productId;
    private String productName = new String();
    private double productPrice;
    
    public CatalogManager() {
        this.catalog = new ArrayList<Product>();
    }
    
    @PostConstruct
    public void initCatalog() {
        Product p = new Product();
        p.setId(1);
        p.setName("test");
        p.setPrice(1.0);
        this.catalog.add(p);
    }
    
    public ArrayList<Product> getCatalog() {
        return this.catalog;
    }
    
    public void setCatalog(ArrayList<Product> catalog) {
        this.catalog = catalog;
    }
    
    public int getProductId() {
        return this.productId;
    }
    
    public void setProductId(int productId) {
        this.productId = productId;
    }
    
    public String getProductName() {
        return this.productName;
    }
    
    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public double getProductPrice() {
        return this.productPrice;
    }
    
    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
    
    public String createProduct() {
        Product p = new Product();
        p.setId(this.productId);
        p.setName(this.productName);
        p.setPrice(this.productPrice);
        this.catalog.add(p);
        return "addProductToCatalog";
    }
}
