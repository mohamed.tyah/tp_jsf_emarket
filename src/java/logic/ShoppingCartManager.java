/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Product;
import model.ShoppingCartItem;

/**
 *
 * @author mac
 */
@ManagedBean
@SessionScoped
public class ShoppingCartManager implements Serializable {
    private ArrayList<ShoppingCartItem> shoppingList;
    private Product prodToAdd;
    private ShoppingCartItem itemToChange;
    private int totalPrice;
    
    public ShoppingCartManager() {
        this.shoppingList = new ArrayList<ShoppingCartItem>();
        this.prodToAdd = new Product();
        this.itemToChange = new ShoppingCartItem();
    }
    
    @PostConstruct
    public void initCart() {
        ShoppingCartItem item = new ShoppingCartItem();
        item.setId(1);
        item.setQuantity(1);
        Product p = new Product();
        p.setId(1);
        p.setName("test");
        p.setPrice(1.0);
        item.setProduct(p);
        this.shoppingList.add(item);
        this.updatePrice();
    }
    
    public ArrayList<ShoppingCartItem> getShoppingList() {
        return this.shoppingList;
    }
    
    public void setCatalog(ArrayList<ShoppingCartItem> shoppingList) {
        this.shoppingList = shoppingList;
    }
    
    public int getTotalPrice() {
        return this.totalPrice;
    }
    
    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
    
    public Product getProdToAdd() {
        return this.prodToAdd;
    }
    
    public void setProdToAdd(Product prodToAdd) {
        this.prodToAdd = prodToAdd;
    }
    
    public ShoppingCartItem getItemToChange() {
        return this.itemToChange;
    }
    
    public void setItemToChange(ShoppingCartItem itemToChange) {
        this.itemToChange = itemToChange;
    }
    
    public String addToCart() {
        ShoppingCartItem itemInArray = this.shoppingList.stream().filter(p -> p.getId() == this.prodToAdd.getId()).findAny().orElse(null);;
        if(itemInArray != null) {
            this.itemToChange = itemInArray;
            this.plusQuantity();
        } else {
            ShoppingCartItem item = new ShoppingCartItem();
            item.setId(this.prodToAdd.getId());
            item.setProduct(this.prodToAdd);
            item.setQuantity(1);
            this.shoppingList.add(item);
        }
        this.updatePrice();
        return "catalogToShoppingCart";
    }
    
    public void removeFromCart() {
        this.shoppingList.remove(this.itemToChange);
        this.updatePrice();
    }
    
    public void plusQuantity() {
        int index = this.shoppingList.indexOf(this.itemToChange);
        ShoppingCartItem element = this.shoppingList.get(index);
        int quantity = element.getQuantity();
        element.setQuantity(quantity + 1);
        this.shoppingList.set(index, element);
        this.updatePrice();
    }
    
    public void minusQuantity() {
        int index = this.shoppingList.indexOf(this.itemToChange);
        ShoppingCartItem element = this.shoppingList.get(index);
        int quantity = element.getQuantity();
        if(quantity == 1) {
            this.removeFromCart();
        } else {
            element.setQuantity(quantity - 1);
            this.shoppingList.set(index, element);
        }
        this.updatePrice();
    }
    
    public void updatePrice() {
        int priceCount = 0;
        for (ShoppingCartItem item : this.shoppingList) { 
            priceCount += item.getProduct().getPrice() * item.getQuantity();
        }
        this.totalPrice = priceCount;
    }
}
